require 'test_helper'

class UsvideosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @usvideo = usvideos(:one)
  end

  test "should get index" do
    get usvideos_url
    assert_response :success
  end

  test "should get new" do
    get new_usvideo_url
    assert_response :success
  end

  test "should create usvideo" do
    assert_difference('Usvideo.count') do
      post usvideos_url, params: { usvideo: {  } }
    end

    assert_redirected_to usvideo_url(Usvideo.last)
  end

  test "should show usvideo" do
    get usvideo_url(@usvideo)
    assert_response :success
  end

  test "should get edit" do
    get edit_usvideo_url(@usvideo)
    assert_response :success
  end

  test "should update usvideo" do
    patch usvideo_url(@usvideo), params: { usvideo: {  } }
    assert_redirected_to usvideo_url(@usvideo)
  end

  test "should destroy usvideo" do
    assert_difference('Usvideo.count', -1) do
      delete usvideo_url(@usvideo)
    end

    assert_redirected_to usvideos_url
  end
end
