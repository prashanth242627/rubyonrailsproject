require "application_system_test_case"

class UsvideosTest < ApplicationSystemTestCase
  setup do
    @usvideo = usvideos(:one)
  end

  test "visiting the index" do
    visit usvideos_url
    assert_selector "h1", text: "Usvideos"
  end

  test "creating a Usvideo" do
    visit usvideos_url
    click_on "New Usvideo"

    click_on "Create Usvideo"

    assert_text "Usvideo was successfully created"
    click_on "Back"
  end

  test "updating a Usvideo" do
    visit usvideos_url
    click_on "Edit", match: :first

    click_on "Update Usvideo"

    assert_text "Usvideo was successfully updated"
    click_on "Back"
  end

  test "destroying a Usvideo" do
    visit usvideos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Usvideo was successfully destroyed"
  end
end
