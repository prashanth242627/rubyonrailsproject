class CreateUsvideos < ActiveRecord::Migration[5.2]
  def change
    create_table :usvideos do |t|
      t.string :video_id
      t.string :title
      t.string :channel_title
      t.integer :category_id
      t.string :tags
      t.integer :views
      t.integer :likes
      t.integer :dislikes
      t.integer :comment_total
      t.string :thumbnail_link
      t.date :date

      t.timestamps
    end
  end
end
