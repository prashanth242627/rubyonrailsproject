class UsvideosController < ApplicationController
  before_action :set_usvideo, only: [:show, :edit, :update, :destroy]

  # GET /usvideos
  # GET /usvideos.json
  def index
    @usvideos = Usvideo.all
  end

  # GET /usvideos/1
  # GET /usvideos/1.json
  def show
  end

  # GET /usvideos/new
  def new
    @usvideo = Usvideo.new
  end

  # GET /usvideos/1/edit
  def edit
  end

  # POST /usvideos
  # POST /usvideos.json
  def create
    @usvideo = Usvideo.new(usvideo_params)

    respond_to do |format|
      if @usvideo.save
        format.html { redirect_to @usvideo, notice: 'Usvideo was successfully created.' }
        format.json { render :show, status: :created, location: @usvideo }
      else
        format.html { render :new }
        format.json { render json: @usvideo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usvideos/1
  # PATCH/PUT /usvideos/1.json
  def update
    respond_to do |format|
      if @usvideo.update(usvideo_params)
        format.html { redirect_to @usvideo, notice: 'Usvideo was successfully updated.' }
        format.json { render :show, status: :ok, location: @usvideo }
      else
        format.html { render :edit }
        format.json { render json: @usvideo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usvideos/1
  # DELETE /usvideos/1.json
  def destroy
    @usvideo.destroy
    respond_to do |format|
      format.html { redirect_to usvideos_url, notice: 'Usvideo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usvideo
      @usvideo = Usvideo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usvideo_params
      params.fetch(:usvideo, {})
    end
end
