json.extract! usvideo, :id, :created_at, :updated_at
json.url usvideo_url(usvideo, format: :json)
