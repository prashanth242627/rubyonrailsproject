require 'csv'

namespace :youtubeusvideos do

  desc "Import Data from Csv"
  task insert_bulk_data: :environment do

  CSV.foreach('USvideos.csv', headers: true, :header_converters => :symbol) do |row|
        attributes = row.to_hash
        Usvideo.create! attributes
  end

  end

  end
