import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule }from "@angular/router"
import { HttpClientModule } from '@angular/common/http';

import { ApiService } from './api.service';

import { AppComponent } from './app.component';
import { YoutubeListComponent } from './youtube-list/youtube-list.component';


@NgModule({
  declarations: [
    AppComponent,
    YoutubeListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'products',
        component: YoutubeListComponent
      }
    ]),
  ],
  providers: [ApiService],
  bootstrap: [AppComponent],


}

)
export class AppModule { }
