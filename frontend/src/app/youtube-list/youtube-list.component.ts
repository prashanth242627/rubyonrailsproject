import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Product } from '../product';


@Component({
  selector: 'app-youtube-list',
  templateUrl: './youtube-list.component.html',
  styleUrls: ['./youtube-list.component.css']
})
export class YoutubeListComponent implements OnInit {

  public columns = ['id', 'created_at', 'updated_at', 'url'];
  public rows : Array<Product>;
  constructor(public apiService: ApiService, public router : Router) { }

  ngOnInit() {
    this.apiService.get("usvideos.json").subscribe((data : Product[]) => {
      console.log(data);
      this.rows = data;
    });
  }

}
